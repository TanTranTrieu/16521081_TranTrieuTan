#include "Bezier.h"
#include <iostream>
#include <cmath>
using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	// de Casteljau's Algorithms
	double t = 0;
	while(t <= 1)
	{
		double x = (1-t)*(1-t)*p1.x + 2*t*(1-t)*p2.x + t*t*p3.x;
		double y = (1-t)*(1-t)*p1.y + 2*t*(1-t)*p2.y + t*t*p3.y;
		SDL_RenderDrawPoint(ren, static_cast<int>(x), static_cast<int>(y));
		t += 0.0001d;
	}
}

void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
    double t = 0;
	while(t <= 1)
	{
		double x = (1-t)*(1-t)*(1-t)*p1.x + 3*t*(1-t)*(1-t)*p2.x + 3*t*t*(1-t)*p3.x + t*t*t*p4.x;
		double y = (1-t)*(1-t)*(1-t)*p1.y + 3*t*(1-t)*(1-t)*p2.y + 3*t*t*(1-t)*p3.y + t*t*t*p4.y;
		SDL_RenderDrawPoint(ren, static_cast<int>(x), static_cast<int>(y));
		t += 0.0001d;
	}
}


