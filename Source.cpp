#include <iostream>
#include <SDL.h>
#include "Bezier.h"

using namespace std;

const int WIDTH  = 800;
const int HEIGHT = 1000;

SDL_Event event;


int main(int, char**){
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0){
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 1366/4, 768/4, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL){
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
    //DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

    SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL){
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

    SDL_SetRenderDrawColor(ren, 0, 0, 255, 0);
	SDL_RenderClear(ren);

    //YOU CAN INSERT CODE FOR TESTING HERE

    Vector2D p1(22, 215), p2(94, 43), p3(140, 258), p4(213, 150);
    SDL_Rect *rect1 = new SDL_Rect();
    rect1->x = p1.x - 3*2;
    rect1->y = p1.y - 3*2;
    rect1->w = 16;
    rect1->h = 16;

    SDL_Rect *rect2 = new SDL_Rect();
    rect2->x = p2.x - 3*2;
    rect2->y = p2.y - 3*2;
    rect2->w = 16;
    rect2->h = 16;

    SDL_Rect *rect3 = new SDL_Rect();
    rect3->x = p3.x - 3*2;
    rect3->y = p3.y - 3*2;
    rect3->w = 16;
    rect3->h = 16;

    SDL_Rect *rect4 = new SDL_Rect();
    rect4->x = p4.x - 3*2;
    rect4->y = p4.y - 3*2;
    rect4->w = 16;
    rect4->h = 16;

    SDL_Color colorCurve = {100, 20, 40, 255}, colorRect = {0, 255, 40, 255};


    //Take a quick break after all that hard work
    //Quit if happen QUIT event
	bool running = true;
	int x = 0;
	int y = 0;
	int area = 0;
	while(running)
	{
        //If there's events to handle
        if(SDL_PollEvent( &event))
        {
            //HANDLE MOUSE EVENTS!!!

			if (SDL_GetMouseState(&x, &y)&SDL_BUTTON(SDL_BUTTON_LEFT))
			{
//				std::cout << x << " " << y << std::endl;

				if((p1.x <= x && x <= p1.x+16) && (p1.y <=y && y <= p1.y+16) )
				{
					p1.x= x;
					p1.y= y;
					area = 1;
				}
				else if(area == 1){
					p1.x= x;
					p1.y= y;
				}
				if(p2.x <= x && x <= p2.x+16 && p2.y <=y && y <= p2.y+16 )
				{
					p2.x= x;
					p2.y= y;
					area=2;
				}
				else if(area == 2){
					p2.x= x;
					p2.y= y;
				}
				if(p3.x <= x && x <= p3.x+16 && p3.y <=y && y <= p3.y+16 )
				{
					p3.x= x;
					p3.y= y;
					area =3;
				}
				else if(area == 3){
					p3.x= x;
					p3.y= y;
				}
				if(p4.x <= x && x <= p4.x+16 && p4.y <=y && y <= p4.y+16 )
				{
					p4.x= x;
					p4.y= y;
					area = 4;
				}
				else if(area == 4){
					p4.x= x;
					p4.y= y;
				}
				//
				rect1->x = p1.x - 3*2;
				rect1->y = p1.y - 3*2;
				rect1->w = 16;
				rect1->h = 16;
				//
				rect2->x = p2.x - 3*2;
				rect2->y = p2.y - 3*2;
				rect2->w = 16;
				rect2->h = 16;
				//
				rect3->x = p3.x - 3*2;
				rect3->y = p3.y - 3*2;
				rect3->w = 16;
				rect3->h = 16;
				//
				rect4->x = p4.x - 3*2;
				rect4->y = p4.y - 3*2;
				rect4->w = 16;
				rect4->h = 16;


				//
			}
			if(event.type==SDL_MOUSEBUTTONUP){
				area = 0;
			}

            //If the user has Xed out the window
            if( event.type == SDL_QUIT )
            {
                //Quit the program
                running = false;
            }
        }
        //
		SDL_SetRenderDrawColor( ren, 0, 0, 0, 255 );
		SDL_RenderClear(ren);
        //
		SDL_SetRenderDrawColor(ren, colorRect.r,colorRect.g,colorRect.b,colorRect.a);
        SDL_RenderDrawRect(ren,rect1);
        SDL_RenderDrawRect(ren,rect2);
        SDL_RenderDrawRect(ren,rect3);
        SDL_RenderDrawRect(ren,rect4);

        //
		SDL_SetRenderDrawColor(ren, 184, 134, 11, 255);
		DrawCurve2(ren,p1,p2,p3);
		//
		SDL_SetRenderDrawColor(ren, 0, 220, 255, 255);
		DrawCurve3(ren,p1,p2,p3,p4);
		//
		SDL_RenderPresent(ren);

    }
	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();
	return 0;
}
