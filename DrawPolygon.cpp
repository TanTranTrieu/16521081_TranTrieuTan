#include "DrawPolygon.h"
#include <iostream>
using namespace std;
const float Pi = 3.14;
void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
  int x[3];
  int y[3];
  float Phi = Pi/4;
  for(int i=0;i<3;++i)
  {
  	x[i] = xc+int(R*cos(Phi)+0.5);
  	y[i] = yc-int(R*sin(Phi)+0.5);
  	//std::cout << x[i] <<  " " << y[i] << std::endl;
  	Phi += 2*Pi/3;
  }

  for(int i=0;i<3;++i)
  {
  	Midpoint_Line(x[i], y[i], x[(i+1)%3], y[(i+1)%3], ren);
  }

}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[4], y[4];
	float Phi = Pi/4;
	for(int i=0;i<4;++i)
	{
		x[i] = xc+int(R*cos(Phi)+0.5);
		y[i] = yc-int(R*sin(Phi)+0.5);
		Phi += 2*Pi/4;
	}

	for(int i=0;i<4;++i)
	{
		DDA_Line(x[i], y[i],x[(i+1)%4], y[(i+1)%4], ren);
	}
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	float Phi = Pi/5;
	for(int i=0;i<5;++i)
	{
		x[i] = xc+int(R*cos(Phi)+0.5);
		y[i] = yc-int(R*sin(Phi)+0.5);
		Phi += 2*Pi/5;
	}

	for(int i=0;i<5;++i)
	{
		DDA_Line(x[i], y[i],x[(i+1)%5], y[(i+1)%5], ren);
	}
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[6], y[6];
	float Phi = Pi/6;
	for(int i=0;i<6;++i)
	{
		x[i] = xc+int(R*cos(Phi)+0.5);
		y[i] = yc-int(R*sin(Phi)+0.5);
		Phi += 2*Pi/6;
	}

	for(int i=0;i<6;++i)
	{
		DDA_Line(x[i], y[i],x[(i+1)%6], y[(i+1)%6], ren);
	}
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5];
    int y[5];

    float Phi = Pi/2;
    for (int i = 0; i < 5; i++)
    {
        x[i] = xc + int (R*cos(Phi) + 0.5);
        y[i] = yc - int (R*sin(Phi) + 0.5);
        Phi += 2*Pi/5;

        //cout << x[i] << " "<< y[i] << " ";
    }

    for (int i = 0; i < 5; i++)
    {
        DDA_Line (x[i], y[i], x[(i + 2) % 5], y[(i + 2)% 5], ren);
    }
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], xp[5];
    int y[5], yp[5];

    float Phi = Pi/2;
    float rnho = R*sin(Pi/10)/ sin (7*Pi/10);
    for (int i = 0; i < 5; i++)
    {
        x[i] = xc + int (R*cos(Phi) + 0.5);
        y[i] = yc - int (R*sin(Phi) + 0.5);

        xp[i] = xc + int (rnho * cos (Phi + Pi/5) + 0.5);
        yp[i] = yc - int (rnho * sin (Phi + Pi/5) + 0.5);

        Phi += 2*Pi/5;

    }
    for (int i = 0; i < 5; i++)
    {
        DDA_Line(x[i], y[i], xp[i], yp[i], ren);
        DDA_Line(xp[i], yp[i], x[(i + 1) % 5], y[(i + 1)% 5],ren);
    }
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[8], xp[8];
    int y[8], yp[8];

    float Phi = 0;
    float rnho = R*sin(Pi/8)/ sin (6*Pi/8);
    for (int i = 0; i < 8; i++)
    {
        x[i] = xc + int (R*cos(Phi) + 0.5);
        y[i] = yc - int (R*sin(Phi) + 0.5);

        xp[i] = xc + int (rnho * cos (Phi + Pi/8) + 0.5);
        yp[i] = yc - int (rnho * sin (Phi + Pi/8) + 0.5);

        Phi += 2*Pi/8;

    }
    for (int i = 0; i < 8; i++)
    {
        DDA_Line(x[i], y[i], xp[i], yp[i], ren);
        DDA_Line(xp[i], yp[i], x[(i + 1) % 8], y[(i + 1)% 8],ren);
    }
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	int x[5], xp[5];
    int y[5], yp[5];

    float Phi = startAngle;
    float rnho = R*sin(Pi/10)/ sin (7*Pi/10);
    for (int i = 0; i < 5; i++)
    {
        x[i] = xc + int (R*cos(Phi) + 0.5);
        y[i] = yc - int (R*sin(Phi) + 0.5);

        xp[i] = xc + int (rnho * cos (Phi + Pi/5) + 0.5);
        yp[i] = yc - int (rnho * sin (Phi + Pi/5) + 0.5);

        Phi += 2*Pi/5;

    }
    for (int i = 0; i < 5; i++)
    {
        DDA_Line(x[i], y[i], xp[i], yp[i], ren);
        DDA_Line(xp[i], yp[i], x[(i + 1) % 5], y[(i + 1)% 5],ren);
    }
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	float banKinh = r;
    float gbd = Pi/2;
    while (banKinh > 1)
    {
        DrawStarAngle(xc,yc,int (banKinh + 0.5), gbd,ren);
        gbd = gbd + M_PI;
        banKinh = banKinh * sin(Pi/10) / sin (7*Pi/10);
    }
}
